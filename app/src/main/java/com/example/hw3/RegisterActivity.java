package com.example.hw3;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class RegisterActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final Calendar myCalendar = Calendar.getInstance();
        EditText birthdate= findViewById(R.id.birthRegEditText);

        Button btnReg = findViewById(R.id.btnRegister2);

        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(RegisterActivity.this, MainActivity.class);
                btnReg.setClickable(true);
                btnReg.setFocusable(true);
                Toast.makeText(RegisterActivity.this, "You registered!", Toast.LENGTH_SHORT).show();
                startActivity(mainIntent);
            }
        });

    }

}