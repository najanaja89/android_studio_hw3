package com.example.hw3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



        Button btnLogin = findViewById(R.id.btnLogIn2);
        EditText phone = findViewById(R.id.phoneLogInEditText);
        EditText pin = findViewById(R.id.pinLogInEditText);
        btnLogin.setBackgroundColor(Color.RED);
        btnLogin.setClickable(false);
        btnLogin.setFocusable(false);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(phone.getText().toString()) && !TextUtils.isEmpty(pin.getText().toString()))
                {
                    Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
                    btnLogin.setClickable(true);
                    btnLogin.setFocusable(true);
                    Toast.makeText(LoginActivity.this, "You logget in!", Toast.LENGTH_SHORT).show();
                    startActivity(mainIntent);
                }

            }
        });

    }
}